﻿/*
 * Yarely Araya.
 * Ejercicios de Lógica y Programación.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Nombre del proyecto.
namespace Ejercicios_Logica
{
    //Clase.
    class Program
    {
        static void Main(string[] args)
        {
            //Variables.
            int opc = 1; //OJO para que empiece desde del 1
            //Ciclo do while para el menú.
            do
            {
                Console.Clear();
                Console.WriteLine("");
                Console.WriteLine("****************************MENÚ****************************");
                Console.WriteLine("" +
                    "\n 1. Calcular el pago neto de un trabajador." +
                    "\n 2. Suma de dos números enteros." +
                    "\n 3. Resta de dos números enteros." +
                    "\n 4. Multiplicación de dos números enteros." +
                    "\n 5. División de dos números enteros." +
                    "\n 6. Calcular la velocidad en (m/s) del corredor en la carrera de 1500 metros." +
                    "\n 7. Calcular la superficie del triángulo." +
                    "\n 8. Calcular el salario semanal de un trabajador." +
                    "\n 9. Intercambio de valores." +
                    "\n10. Calcular la distancia recorrida de un automóvil." +
                    "\n11. Calcular el promedio de un estudiante." +
                    "\n12. Calcular el número de respuestas correctas, incorrectas y en blanco." +
                    "\n13. Calcular el número de CDs necesarios para hacer una copia de seguridada." +
                    "\n14. Salir." +
                    "\n" +
                    "" +
                    "***********************************************************");
                Console.WriteLine("Digite una opción del menú. ");
                opc = Convert.ToInt32(Console.ReadLine());
                switch (opc)
                {
                    case 1:
                        cls_Metodos obj = new cls_Metodos();
                        obj.PagoNeto();
                        break;
                    case 2:
                        cls_Metodos obj1 = new cls_Metodos();
                        obj1.Suma();
                        break;
                    case 3:
                        cls_Metodos obj2 = new cls_Metodos();
                        obj2.Resta();
                        break;
                    case 4:
                        cls_Metodos obj3 = new cls_Metodos();
                        obj3.Multiplicacion();
                        break;
                    case 5:
                        cls_Metodos obj4 = new cls_Metodos();
                        obj4.Division();
                        break;
                    case 6:
                       /* Clase_Metodos obj5 = new Clase_Metodos();
                        obj5.marte();*/
                        break;
                    case 7:
                        cls_Metodos obj6 = new cls_Metodos();
                        obj6.Triangulo();
                        break;
                    case 8:
                        /*Clase_Metodos obj7 = new Clase_Metodos();
                        obj7.longitudSombra();*/
                        break;
                    case 9:
                        cls_Metodos obj8 = new cls_Metodos();
                        obj8.Intercambio();
                        break;
                    case 10:
                        /*Clase_Metodos obj9 = new Clase_Metodos();
                        obj9.promedio();*/
                        break;
                    case 11:
                        cls_Metodos obj10 = new cls_Metodos();
                        obj10.Promedio();
                        break;
                    case 12:
                        /*Clase_Metodos obj9 = new Clase_Metodos();
                        obj9.promedio();*/
                        break;
                    case 13:
                        /*Clase_Metodos obj9 = new Clase_Metodos();
                        obj9.promedio();*/
                        break;
                    case 14:
                        Console.WriteLine(" ¡GRACIAS POR USAR NUESTRO SISTEMA! ");
                        break;
                    default:
                        Console.WriteLine(" ¡OPCIÓN INCORRECTA! " +
                            "\n Digite una opción entre 1 y 14.");
                        break;
                }
                Console.ReadKey();
            } while (opc != 14); //Fin del método do.
        }
    }
}
