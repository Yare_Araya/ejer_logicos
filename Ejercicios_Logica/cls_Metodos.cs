﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Logica
{
    class cls_Metodos
    {
        #region PAGO NETO.
        //Método.
        public double PagoNeto()
        {
            //Variables.
            int horas;
            double tarifa, impuesto, Result;
            
            Console.WriteLine("Digite el número de horas trabajadas:");
            horas = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Digite la tarifa a pagar por hora:");
            tarifa = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Digite el porcentaje de tasa de impuestos a reducir:");
            impuesto = Convert.ToDouble(Console.ReadLine());

            //Calculo.
            Result = (horas * tarifa);
            impuesto =  (impuesto/100);
            impuesto = (Result * impuesto);
            Result = (Result - impuesto);
                       
            //Respuesta.
            Console.WriteLine("El pago neto del trabajador sería de: " +Result);
            
            return 0.0;
        }
        #endregion

        #region SUMA.
        //Método.
        public int Suma()
        {
            //Variables.
            int valor1, valor2, Result;

            Console.WriteLine("Digite la primer cifra a SUMAR:");
            valor1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Digite la segunda cifra a SUMAR:");
            valor2= Convert.ToInt32(Console.ReadLine());

            //Calculo.
            Result = (valor1 + valor2);

            //Resultado.
            Console.WriteLine("El resultado de la SUMA es de: " + Result);

            return 0;
        }
        #endregion

        #region RESTA.
        //Método.
        public int Resta()
        {
            //Variables.
            int valor1, valor2, Result;

            Console.WriteLine("Digite la primer cifra a RESTAR:");
            valor1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Digite la segunda cifra a RESTAR:");
            valor2 = Convert.ToInt32(Console.ReadLine());

            //Calculo.
            Result = (valor1 - valor2);

            //Resultado.
            Console.WriteLine("El resultado de la RESTA es de: " + Result);

            return 0;
        }
        #endregion

        #region MULTIPLICACIÓN.
        //Método.
        public int Multiplicacion()
        {
            //Variables.
            int valor1, valor2, Result;

            Console.WriteLine("Digite la primer cifra a MULTIPLICAR:");
            valor1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Digite la segunda cifra a MULTIPLICAR:");
            valor2 = Convert.ToInt32(Console.ReadLine());

            //Calculo.
            Result = (valor1 * valor2);

            //Resultado.
            Console.WriteLine("El resultado de la MULTIPLICACIÓN es de: " + Result);

            return 0;
        }
        #endregion

        #region DIVISIÓN.
        //Método.
        public int Division()
        {
            //Variables.
            int valor1, valor2, Result;

            Console.WriteLine("Digite la primer cifra a DIVIDIR:");
            valor1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Digite la segunda cifra a DIVIDIR:");
            valor2 = Convert.ToInt32(Console.ReadLine());

            //Calculo.
            if (valor2 == 0)
            {
                Console.WriteLine("ERROR, No se puede dividir entre 0. ");
            }
            else {
                Result = (valor1 / valor2);
                Console.WriteLine("El resultado de la DIVISIÓN es de: " + Result);
            }
            return 0;
        }
        #endregion

        #region TRIÁNGULO.
        //Método.
        public double Triangulo()
        {
            //Variables.
            double Base, altura, area;

            Console.WriteLine("Digite la BASE del Triángulo:");
            Base = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Digite la ALTURA del Triángulo:");
            altura = Convert.ToDouble(Console.ReadLine());

            //Calculo.
            area = (Base * altura) / 2;

            //Respuesta.
            Console.WriteLine("El ÁREA del Triángulo es de: " + area);
            return 0.0;
        }
        #endregion

        #region INTERCAMBIO.
        //Método.
        public double Intercambio()
        {
            //Variables.
            Double v1, v2;

            Console.WriteLine("Digite el valor de la PRIMER variable:");
            v1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Digite el valor de la SEGUNDA variable:");
            v2 = Convert.ToDouble(Console.ReadLine());

            //Respuesta.
            Console.WriteLine("El valor de la PRIMER variable es de: " + v2);
            Console.WriteLine("El valor de la SEGUNDA variable es de: " + v1); 

            return 0.0;
        }
        #endregion

        #region PROMEDIO ESTUDIANTES.
        //Método.
        public double Promedio()
        {
            //Variables.
            Double n1, n2, n3, Result;

            Console.WriteLine("Digite la NOTA obtenida en la PRIMERA prueba:");
            n1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Digite la NOTA obtenida de la SEGUNDA prueba:");
            n2 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Digite la NOTA obtenida de la TERCERA prueba:");
            n3 = Convert.ToDouble(Console.ReadLine());

            //Calculo.

            Result = (n1 + n2 + n3)/3;

            //Respuesta.
            Console.WriteLine("\nEl Promedio Final del estudiante SIN REDONDEAR es de: " + Result);   //33,3% *85=25.05  33,3% * 72.3 = 23.859   33,3% *82.51= 27.2283  //79.85673
            Console.WriteLine("El Promedio Final REDONDEADO del estudiante es de: " + Math.Round(Result));

            return 0.0;
        }
        #endregion



    }
}
